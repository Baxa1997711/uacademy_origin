import React from "react";
import SingleBanner from "components/UI/Banner/SingleBanner/SingleBanner";
import StepInfo from "components/UI/StepInfo/StepInfo";
import Blog from "../../UI/Blog/Blog";
import styles from "./single.module.scss";
import Teachers from "components/UI/Teachers/Teachers";
import Process from "components/UI/Process/Process";
import Advantages from "components/UI/Advantages/Advantages";
import StudyPlan from "components/UI/StudyPlan/StudyPlan";
import Certificate from "components/UI/Certificate/Certificate";
import Employers from "components/UI/Employers/Employers";
import Feedback from "components/UI/Feedback/Feedback";
import SingleForm from "components/UI/SingleForm/SingleForm";
import Question from "components/UI/Question/Question";

function Single({ major }) {
  return (
    <div className={styles.single}>
      <SingleBanner bannerInfo={major?.banner} />
      <StepInfo />
      <Blog blog={major?.blog} />
      <Teachers teachers={major?.teachers} />
      <Process />
      <Advantages />
      <StudyPlan program={major?.program} />
      <Certificate />
      <Employers />
      <div className={styles.feedback}>
        <Feedback />
      </div>
      <SingleForm />
      <Question question={major?.questions} />
    </div>
  );
}

export default Single;
