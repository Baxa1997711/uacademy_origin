import React, {useState} from 'react';
import Image from 'next/image'
import styles from './studyplan.module.scss'
import { Container,  } from '@mui/material';
import { Accordion, AccordionSummary, Typography, AccordionDetails} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { PlusIcon, AccordionCancel } from '../../svg.js'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles({
  
})


function Studyplan({ program }) {
      const classes = useStyles();
    
      const [expanded, setExpanded] = useState('');
      const handleChange = (isExpanded, panel) => {
        setExpanded(isExpanded ? panel : false)
      }
      
      // const [  ]
    return (
        <div className={styles.studyPlan}>
          <Container className='container'>
              <h2 className={styles.studyPlan_title}>3-месячная программа</h2>
              <div className={styles.studyPlan_content} id='study_plan'>
               {program?.map(item => (
                    <Accordion key={item?.id} id='accordion' className={styles.accordion} expanded={expanded === `panel${item?.id}`} onChange={(event, isExpanded) => handleChange(isExpanded, `panel${item?.id}`)}>
                    <AccordionSummary
                    expandIcon={expanded === `panel${item?.id}`?<AccordionCancel />:<PlusIcon/>}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                    >
                      <Typography className={styles.accordion_title} sx={{ width: '33%', flexShrink: 0 }}>
                      {item?.title}
                    </Typography>
                    </AccordionSummary>
                    <AccordionDetails  className={styles.accordion_details}>
                    <Typography className={styles.accordion_content}>
                    <ul className=''>
                      { item?.types.map(item => (
                        <li key={item?.id}>{item}</li>
                      )) }
                    </ul>
                    </Typography>
                    </AccordionDetails>
                    </Accordion>
               ))} 
              </div>
            </Container>  
        </div>
    );
}

export default Studyplan;