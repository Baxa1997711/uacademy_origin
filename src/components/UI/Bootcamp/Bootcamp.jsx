import { Container } from '@mui/material';
import React from 'react';
import styles from './bootcamp.module.scss'
import { 
    BootcampIcon1,
    BootcampIcon2,
    BootcampIcon3,
    BootcampIcon4
 } 
from '../../svg.js';
import BootcampImg from '../../../../public/images/Bootcamp_banner.png'
import Image from 'next/image'


const data = [
    {
      id: 0,
      icon: <BootcampIcon1/>,  
      title: '80% практики'
    },
    {
      id: 1,
      icon: <BootcampIcon2/>,  
      title: 'Менторы с опытом разработки более 2 года'
    },
    {
      id: 2,
      icon: <BootcampIcon3/>,  
      title: 'практические задание из реалных проектов'
    },
    {
      id: 3,
      icon: <BootcampIcon4/>,  
      title: 'Портфолио и опыт работы в команде по окончанию'
    },
]


function Bootcamp(props) {
    return (
        <div className={styles.bootcamp}>
            <Container className='container'>
                <h2 className={styles.bootcamp_title}>Система обучения в Буткемп</h2>
                
                <div className={styles.bootcamp_content}>
                    <div className={styles.bootcamp_context}>
                        {data?.map(item => (
                            <div key={item?.id} className={styles.bootcamp_context_item}>
                                <div className={styles.bootcamp_context_icon}>
                                {item?.icon}
                                </div>
                                <p>{item?.title}</p>
                            </div>  
                        ))} 
                        
                    </div>
                    <div className={styles.bootcamp_infoImg}>
                        <Image
                        src={BootcampImg}
                        alt=''
                        width={506}
                        height={463}
                        />
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default Bootcamp;