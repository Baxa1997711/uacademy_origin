import { Container } from "@mui/material";
import React, { useState } from "react";
import styles from "./feedback.module.scss";
import Image from "next/image";
import { Button, Dialog } from "@mui/material";
import { PlayIcon, CloseIcon } from "../../svg.js";
import Slider from "react-slick";
import { ArrowLeft, ArrowRight } from "../SlickCarouselArrows/Arrows";

const data = [
  {
    id: 0,
    img: "/images/graduates_img.png",
    name: "Юлдуз Раджабова",
  },
  {
    id: 1,
    img: "/images/graduates_img2.png",
    name: "Андрей Ершов",
  },
  {
    id: 2,
    img: "/images/graduates_img3.png",
    name: "Михаил Бин",
  },
  {
    id: 3,
    img: "/images/graduates_img4.png",
    name: "Юлдуз Раджабова",
  },
  {
    id: 2,
    img: "/images/graduates_img3.png",
    name: "Михаил Бин",
  },
  {
    id: 2,
    img: "/images/graduates_img3.png",
    name: "Михаил Бин",
  },
  {
    id: 4,
    img: "/images/graduates_img4.png",
    name: "Юлдуз Раджабова",
  },
  {
    id: 4,
    img: "/images/graduates_img4.png",
    name: "Юлдуз Раджабова",
  },
];

function Graduates(props) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    dotsClass: "slick-dots",
    // arrows: true,
    nextArrow: <ArrowRight styles={styles.nextButton} />,
    prevArrow: <ArrowLeft styles={styles.prevButton} />,
  };
  
  const [open, setOpen] = useState(false);
  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div className={styles.graduates} id='feedback'>
      <Container className="container">
        <h2 className="title">Выпускники Буткэмпа</h2>
        
        <div className={styles.graduates_content}>
          <Slider {...settings} className="graduates_slider">
            {data?.map((item) => (
              <div key={item?.id} className={styles.graduates_content_item} onClick={handleClick}>
                <div className={styles.graduates_content_img}>
                  <Image src={item?.img} alt="" width={294} height={439} />
                </div>

                <div className={styles.graduates_control_panel}>
                  <Button onClick={handleClick} className={styles.graduates_play_btn}>
                    <span><PlayIcon />Смотреть видео</span>
                  </Button>
                  <h3>{item?.name}</h3>
                </div>
              </div>
            ))}
          </Slider>
        </div>
        
        <Dialog open={open} onClose={handleClose} id='feedback_dialog'> 
        <button onClick={handleClose} className={styles.close_icon}><CloseIcon/></button>
          <div className={styles.feedback_dialog}>
          <iframe width="100%" height="100%" className={styles.iframe} src="https://www.youtube.com/embed/Fkd9TWUtFm0" title="Томас Суарез: 12-летний разработчик приложений" frame-border="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
          </div>
        </Dialog>
        
      </Container>
    </div>
  );
}

export default Graduates;
