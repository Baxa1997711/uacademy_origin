import { Container } from '@mui/material';
import React from 'react';
import styles from './features.module.scss'
import { 
    FeatureIcon1,
    FeatureIcon2,
    FeatureIcon3,
    FeatureIcon4 
}
 from '../../svg';
 import Image from 'next/image'
 

const data = [
    {
      id: 0,
      img: <FeatureIcon1/>,
      title: '20+  реальных крупных проектов',
      subtitle: 'является одним из видов обеспечения '  
    },
    {
      id: 1,
      img: <FeatureIcon2/>,
      title: 'Лучшие эксперты своего дела',
      subtitle: 'является одним из видов обеспечения '  
    },
    {
      id: 2,
      img: <FeatureIcon3/>,
      title: 'Поддержка всех проектов',
      subtitle: 'является одним из видов обеспечения '  
    },
    {
      id: 3,
      img: <FeatureIcon4/>,
      title: 'Всплоченная работа и дружный коллектив',
      subtitle: 'является одним из видов обеспечения '  
    },
]

function Features(props) {
    return (
        <div className={styles.features}>
            <Container className='container'>
                <h2 className='title'>Почему выберают нас</h2>
                <div className={styles.features_content}>
                    {data?.map(item => (
                       <div key={item?.id} className={styles.features_content_item}>
                         <div className={styles.features_item_icon}>
                           { item?.img }
                          </div>
                       <div className={styles.features_context}>
                           <h4>{item?.title}</h4>
                           <p>{item?.subtitle}</p>
                       </div>
                   </div> 
                    ))}
                </div>
            </Container>
        </div>
    );
}

export default Features;