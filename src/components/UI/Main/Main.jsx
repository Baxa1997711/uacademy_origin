import Image from 'next/image'
import styles from './Main.module.scss'
import useTranslation from 'next-translate/useTranslation'
import { Button } from '@mui/material'
import { useEffect, useState } from 'react'
import { Container } from '@mui/material';
import Banner from '../Banner/Banner'
import Features from '../Features/Features'
import Courses from '../Courses/Courses'
import Numbers from '../Numbers/Numbers'
import Bootcamp from '../Bootcamp/Bootcamp'
import Feedback from '../Feedback/Feedback'
import Form from '../Form/Form'

export function Main() {
  const { t } = useTranslation('common')
  
  const [posts, setPosts] = useState([])

  useEffect(() => {
    // getPosts({ limit: 10, page: 1 }).then((res) => {
    //   setPosts(res)
    // })
  }, [])

  const addPost = () => {
    createPost(
      JSON.stringify({
        title: 'foo',
        body: 'bar',
        userId: 1,
      })
    ).then((res) => {
      console.log('create')
    })
  }

  return (
    <main className={styles.main}>
      <Banner/>
      <Features/>
      <Courses/>
      <Numbers/>
      <Bootcamp/>
      <Feedback/>
      <Form/>
    </main>
  )
}
