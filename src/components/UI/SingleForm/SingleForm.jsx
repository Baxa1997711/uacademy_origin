import React from "react";
import { Container, Button } from "@mui/material";
import Image from "next/image";

import styles from "./singleform.module.scss";

function SingleForm(props) {
  return (
    <div className={styles.singleForm} id="single_form">
      <Container className="container">
        <div className={styles.singleForm_content}>
          <div className={styles.singleForm_context}>
            <h2>Хочу стать дизайнером</h2>
            <p>
              Если у вас есть вопросы о формате или вы не знаете, что
              выбрать,оставьте свой номер и наши операторы вам перезвонят
            </p>
          </div>
          <form action="" className={styles.form}>
            <input
              type="text"
              placeholder="Введите имя"
              className={styles.form_controls}
            />
            <input
              type="text"
              placeholder="Введите телефон"
              className={styles.form_controls}
            />
            <Button className={styles.form_btn}>Записаться на курс</Button>
          </form>
        </div>
      </Container>
    </div>
  );
}

export default SingleForm;
