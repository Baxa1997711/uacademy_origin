import React, { useState } from "react";
import Image from "next/image";
import { Container, Box, Button, Dialog } from "@mui/material";

import styles from "./single.module.scss";
import { CloseIcon } from "../../../svg.js";

function SingleBanner({ bannerInfo }) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div className={styles.singleBanner}>
      <Container className="container">
        <div className={styles.banner_text}>
          <Box>
            <h1 className={styles.banner_title}>{bannerInfo?.title}</h1>
            <p className={styles.banner_subtitle}>{bannerInfo?.subtitle}</p>
            <Button onClick={handleOpen} className={styles.banner_btn}>
              Подать заявку
            </Button>
          </Box>
          <Box>
            <Image
              src={bannerInfo?.bannerImage}
              alt=""
              width={577}
              height={380}
            />
          </Box>
        </div>

        <Dialog open={open} onClose={handleClose} id="dialog">
          <div className={styles.dialog_content}>
            <button onClick={handleClose} className={styles.close_icon}>
              <CloseIcon />
            </button>

            <div className={styles.dialog_context}>
              <h2>Заполните форму, чтобы оформить заказ</h2>
              <p>
                Если у вас есть вопросы о формате или вы не знаете, что
                выбрать,оставьте свой номер и наши операторы вам перезвонят
              </p>

              <form action="">
                <input type="text" placeholder="Введите имя" />
                <input type="text" placeholder="Введите телефон" />
                <button>Записаться на курс</button>
              </form>
            </div>
          </div>
        </Dialog>
      </Container>

      <Image
        src={bannerInfo?.bannerBg}
        priority={true}
        alt="cspace"
        layout="fill"
      />
    </div>
  );
}

export default SingleBanner;
