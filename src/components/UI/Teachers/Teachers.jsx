import React, { useRef } from "react";
import Image from "next/image";
import Slider from "react-slick";
import { style } from "@mui/system";
import { Container } from "@mui/material";

import styles from "./teacher.module.scss";
import { TeachersBg, ArrowPrevIcon, ArrowNextIcon } from "../../svg.js";

function Teachers({ teachers }) {
  const settings = {
    fade: true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    dotsClass: "slick-dots",
    arrows: false,
  };

  const slider = useRef();

  const previous = () => {
    slider.current.slickPrev();
  };

  const next = () => {
    slider.current.slickNext();
  };

  return (
    <div className={styles.teachers}>
      <Container className="container">
        <Slider {...settings} ref={slider} className="teachers_slider">
          {teachers.map((item) => (
            <div key={item?.id} className={styles.teachers_content}>
              <div className={styles.teachers_left}>
                <h2>Наши Учителя</h2>
                <p>{item.title}</p>
                <h4>{item.name}</h4>
              </div>
              <div className={styles.teachers_right}>
                <div className={styles.teachers_right_bg}>
                  <TeachersBg />
                </div>
                {teachers.length > 1 && (
                  <div className={styles.teachers_right_btns}>
                    <button
                      onClick={previous}
                      className={styles.teachers_right_btns_pre}
                    >
                      <ArrowPrevIcon />
                    </button>
                    <button
                      onClick={next}
                      className={styles.teachers_right_btns_next}
                    >
                      <ArrowNextIcon />
                    </button>
                  </div>
                )}
                <div className={styles.teachers_right_img}>
                  <Image src={item.img} alt="" layout="fill" />
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </Container>
    </div>
  );
}

export default Teachers;
