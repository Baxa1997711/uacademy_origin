import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";

import Desktop from "./Topbar/Desktop";
import Mobile from "./Topbar/Mobile";
import styles from "./Header.module.scss";

export function Header() {
  const router = useRouter();
  const { t } = useTranslation("common");
  const langs = [
    {
      key: "ru",
      label: "ru",
    },
    {
      key: "uz",
      label: "uz",
    },
    {
      key: "en",
      label: "en",
    },
  ];

  return (
    <header className={styles.header}>
      <Desktop />
      <Mobile />
    </header>
  );
}
