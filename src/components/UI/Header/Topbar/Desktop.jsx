import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { Container, Button, Dialog } from "@mui/material";

import styles from "../Header.module.scss";
import Logo from "../../../../../public/images/Logo.png";
import { PhoneIcon, CloseIcon } from "../../../svg.js";

function Desktop(props) {
  const router = useRouter();

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  
  const handleSubmit = (e) => {
    e.preventDefault();
  }

  return (
    <div className={styles.desktop_topbar}>
      <Container className="container">
        <div className={styles.desktop_topbar_content}>
          <Link href="/">
            <a className={styles.desktop_logo}>
              <Image src={Logo} alt="" />
            </a>
          </Link>
          {/* {router.query.id === undefined ? (
            <div className={styles.topbar_links}>
              <Link href="/#courses">
                <a className={styles.topbar_link}>Все курсы</a>
              </Link>
              <Link href="/#feedback">
                <a className={styles.topbar_link}>Отзывы</a>
              </Link>
              <Link href="/">
                <a className={styles.topbar_link}>Связаться с нами</a>
              </Link>
            </div>
          ) : (
            ""
          )} */}

          <div className={styles.contact_panel}>
              <a href="tel:+998 (71) 202-42-22">
                <PhoneIcon />
                <span>+998 (71) 202-42-22</span>
              </a>

            <Button
              onClick={handleOpen}
              className={styles.register_btn}
              disableRipple
            >
              Записаться
            </Button>
          </div>
        </div>
      </Container>

      <Dialog open={open} onClose={handleClose} id="dialog">
        <div className={styles.dialog_content}>
          <button onClick={handleClose} className={styles.close_icon}>
            <CloseIcon />
          </button>

          <div className={styles.dialog_context}>
            <h2>Заполните форму, чтобы оформить заказ</h2>
            <p>
              Если у вас есть вопросы о формате или вы не знаете, что
              выбрать,оставьте свой номер и наши операторы вам перезвонят
            </p>

            <form action="" onSubmit={handleSubmit}>
              <input type="text" placeholder="Введите имя" />
              <input type="text" placeholder="Введите телефон" />
              <button>Записаться на курс</button>
            </form>
          </div>
        </div>
      </Dialog>
    </div>
  );
}

export default Desktop;
