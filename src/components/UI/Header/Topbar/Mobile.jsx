import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { Container, Drawer } from "@mui/material";

import styles from "../Header.module.scss";
import Logo from "../../../../../public/images/Logo.png";
import { MenuBurgerIcon } from "../../../svg.js";

function Mobile() {
  const [openDrawer, setOpenDrawer] = useState(false);

  const handleOpen = () => setOpenDrawer(true);
  const handleClose = () => setOpenDrawer(false);

  return (
    <>
      <div className={styles.mobile_topbar}>
        <Container>
          <div className={styles.mobile_header}>
            <div>
              <Link href="/">
                <a className={styles.mobile_header_logo}>
                  <Image src={Logo} alt="" width={119.1} height={27} />
                </a>
              </Link>
            </div>
            <div className={styles.mobile_header_btn}>
              {openDrawer || (
                <button onClick={handleOpen}>
                  <MenuBurgerIcon />
                </button>
              )}
            </div>
          </div>
        </Container>
      </div>

      <Drawer open={openDrawer} anchor="left" onClose={handleClose}>
        <div className={styles.mobile_header_drawer}>
          <Link href="/">
            <a className={styles.mobile_header_logo} onClick={handleClose}>
              <Image src={Logo} alt="" width={119.1} height={27} />
            </a>
          </Link>
          <div className={styles.mobile_header_links}>
            <Link href="/">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Все курсы
              </a>
            </Link>
            <Link href="/">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Отзывы
              </a>
            </Link>
            <Link href="/coaches">
              <a className={styles.mobile_header_link} onClick={handleClose}>
                Связаться с нами
              </a>
            </Link>
          </div>
        </div>
      </Drawer>
    </>
  );
}

export default Mobile;
