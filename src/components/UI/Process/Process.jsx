import React from 'react';
import Image from 'next/image'
import styles from './process.module.scss'
import { Container } from '@mui/material';
import ProcessImg from '../../../../public/images/Process_img.png'


function Process(props) {
    return (
        <div className={styles.process}>
           <Container>
                <h2 className={styles.process_title}>Как проходит обучение</h2>
                <div className={styles.process_content}>
                   <div className={styles.process_content_process}>
                        <Image
                        src={ProcessImg}
                        alt=''
                        />                        
                    </div> 
                </div>
            </Container> 
        </div>
    );
}

export default Process;