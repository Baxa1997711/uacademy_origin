import { Container } from '@mui/material';
import React from 'react';
import styles from './form.module.scss'


function Form(props) {
    
    const handleClick = (e) => {
        e.preventDefault();
    }
    
    return (
        <div className={styles.form} id='form'>
            <Container className='container'>
                <div className={styles.form_content}>
                    <div className={styles.form_content_text}>
                        <h2>Не можете выбрать направление?</h2>
                        <p>Если у вас есть вопросы о формате или вы не знаете, что выбрать,оставьте свой номер и наши операторы вам перезвонят</p>
                    </div>
                    
                    <form action="" className={styles.form_panel} onSubmit={handleClick}>
                        <input type="text" placeholder='Введите имя' className={styles.form_input}/>
                        <input type="text" placeholder='Введите телефон' className={styles.form_input}/>
                        <button type='submit'>Отправить</button>
                    </form>
                </div>
            </Container>
        </div>
    );
}

export default Form;