import { Container } from '@mui/material';
import React from 'react';
import styles from './certificate.module.scss'
import Image from 'next/image'
import certificate from '../../../../public/images/certificate.png'


function Certificate(props) {
    return (
        <div className={styles.certificate}>
            <Container className='container'>
                <div className={styles.certificate_content}>
                    <div className={styles.certificate_context}>
                        <h2>Сертификат при прохождении курса</h2>
                        <p>Вы получите Сертификат о профессиональной переподготовке, которые можно добавить в портфолио и показать работодателю.</p>
                    </div>
                    <div className={styles.certificate_img}>
                        <Image
                        src={certificate}
                        alt=''
                        />
                    </div>
                </div>
            </Container>
        </div>
    );
}

export default Certificate;