import { Container } from '@mui/material';
import React, { useState } from 'react';
import styles from './questions.module.scss'
import { Accordion, AccordionDetails, Typography, AccordionSummary } from '@mui/material'
import { PlusIcon, MinusIcon } from '../../svg.js'


function Question({ question }) {
    
    const [expanded, setExpanded] = useState('');
      const handleChange = (isExpanded, panel) => {
        setExpanded(isExpanded ? panel : false)
      }
    return (
        <div className={styles.question}>
            <Container className='container'>
                <h2 className={styles.question_title}>Часто задаваемые вопросы</h2>
                <div className={styles.question_accordion_content} id='question_content'>
                    
                 {question.map(item => (
                    <Accordion key={item?.id} className={styles.accordion} id='accordion' expanded={expanded === `panel${item?.id}`} onChange={(event, isExpanded) => handleChange(isExpanded, `panel${item?.id}`)}>
                    <AccordionSummary
                        expandIcon={expanded === `panel${item?.id}`?<MinusIcon />:<PlusIcon/>}
                        aria-controls="panel1bh-content"
                        id="question_accordion"
                    >
                        <Typography className={styles.question_accordion_title} sx={{ width: '33%', flexShrink: 0 }}>
                            {item?.title}
                        </Typography>
                    </AccordionSummary>
                    <AccordionDetails className={styles.question_accordion_details}>
                        <Typography className={styles.question_accordion_subtitle}>
                            {item?.subtitle}
                        </Typography>
                    </AccordionDetails>
                    </Accordion>
                 ))}   

                </div>
            </Container>
        </div>
    );
}

export default Question;