import styles from './Footer.module.scss'
import Link from 'next/link'
import { Container } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import Image from 'next/image';
import Logo from '../../../../public/images/Logo.png'
import {  
  GoogleIcon, 
  WhatsupIcon,
  InstagramIcon,
  FacebookIcon
}
from '../../svg.js'

export function Footer() {
  const { t } = useTranslation('common')
  
  
  return (
    <div className={styles.footer}>
       <Container className='container'>
            <div className={styles.footer_content}>
                <div className={styles.footer_info}>
                  <Link href="">
                    <a  className={styles.footer_logo}>
                      <Image
                      src={Logo}
                      alt=''
                      />
                    </a>
                  </Link>
                  <p>Венчурный фонд, созданный для развития стартапов на рынке Узбекистана.</p>
                  <div className={styles.footer_socials}>
                    <a href=""><GoogleIcon/></a>
                    <a href=""><WhatsupIcon/></a>
                    <a href=""><InstagramIcon/></a>
                    <a href=""><FacebookIcon/></a>
                  </div>
                </div>
{/*                 
                <div className={styles.context}>
                  <div className={styles.context_item}>
                    <h4>Направления</h4>
                    <div className={styles.context_item_links}>
                      <a href="" className={styles.context_item_link}>Венчурный Фонда</a>
                      <a href="" className={styles.context_item_link}>Акселератор</a>
                      <a href="" className={styles.context_item_link}>Поддержка</a>
                    </div>
                  </div>
                  <div className={styles.context_item}>
                    <h4>Ресурсы</h4>
                    <div className={styles.context_item_links}>
                      <a href="https://www.w3schools.com" className={styles.context_item_link}>Статьи</a>
                      <a href="" className={styles.context_item_link}>Полезные Материалы</a>
                      <a href="" className={styles.context_item_link}>Научные Работы</a>
                    </div>
                  </div>
                  <div className={styles.context_item}>
                    <h4>Компания</h4>
                    <div className={styles.context_item_links}>
                      <a href="" className={styles.context_item_link}>Конфиденциальность</a>
                      <a href="" className={styles.context_item_link}>Условия Пользования</a>
                      <a href="" className={styles.context_item_link}>О нас</a>
                    </div>
                  </div>
                </div> */}
            </div>
        </Container> 
    </div>
  )
}
