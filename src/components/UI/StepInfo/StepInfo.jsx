import { Container } from '@mui/material';
import React from 'react';
import styles from './stepInfo.module.scss'

const data = [
    {
      id: 0,
      title: 'След группа',
      subtitle: '14 июля'  
    },
    {
      id: 1,
      title: 'Длителность',
      subtitle: '4 месяца'  
    },
    {
      id: 2,
      title: 'Началный уровень',
      subtitle: 'для начинаюшых'  
    },
    {
      id: 3,
      title: 'Формат обучение',
      subtitle: 'оффлайн'  
    },
]

function StepInfo() {
    return (
        <div className={styles.stepInfo}>
            <Container className='container'>
                <div className={styles.stepInfo_content}>
                    {data?.map(item => (
                       <div key={item?.id} className={styles.stepInfo_content_item}>
                       <div className={styles.stepInfo_title}>
                           <div className={styles.stepInfo_dot}></div>
                           <h3>{item?.title}</h3>
                       </div>
                       <p className={styles.stepInfo_subtitle}>{item?.subtitle}</p>
                   </div> 
                    ))}
                </div>
            </Container>
        </div>
    );
}

export default StepInfo;