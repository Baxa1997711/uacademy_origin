import { Container } from '@mui/material';
import React from 'react';
import styles from './numbers.module.scss'

const data = [
    {
      id: 0,
      number: '5',
      subtitle: 'Service Geographics'  
    },
    {
      id: 1,
      number: '30',
      subtitle: 'Fortune 500 Clients'  
    },
    {
      id: 2,
      number: '300',
      subtitle: 'Dedicated Members'  
    },
    {
      id: 3,
      number: '8',
      subtitle: 'Years of Journey'  
    },
]

function Numbers(props) {
    return (
        <div className={styles.numbers}>
          <Container className='container'>
            <h2 className='title'>Интересные цифры</h2>
            <div className={styles.numbers_content}>
                {data?.map(item => (
                  <div key={item?.id} className={styles.numbers_content_item}>
                    <h3>{item?.number}</h3>
                    <p>{item?.subtitle}</p>
                  </div>  
                ))}   
            </div>
            </Container>  
        </div>
    );
}

export default Numbers;